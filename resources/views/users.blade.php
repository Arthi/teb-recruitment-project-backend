<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>TEB Laravel</title>
    <script src="{{ mix('js/app.js') }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>
<div id="app">

    <div class="container">
        <h3 class="text-center py-4">Zarządzanie użytkownikami</h3>

        <div class="row justify-content-center">

            <new-user-component></new-user-component>
            <user-list-component></user-list-component>

        </div>
    </div>
</div>
</body>
</html>
