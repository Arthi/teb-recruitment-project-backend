<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/users', [UserController::class, 'create'])->name('users_create');
Route::get('/users', [UserController::class, 'list'])->name('users_list');
Route::get('/users/search', [UserController::class, 'search'])->name('users_search');
Route::get('/users/roles', [UserController::class, 'roles'])->name('users_roles');
