# TEB Recruitment Project - Backend
Projekt rekrutacyjny TEB na stanowisko programisty (codziennie pracuje w symfony, z laravelem nie za bardzo, więc dajcie znać co mogłem zrobić lepiej)

## TLDR
Aby uruchomić projekt, należy w katalogu głównym projektu wykonać polecenia:

```
composer install
php artisan migrate:fresh --seed
npm install & npm run dev
```

## Plik konfiguracji środowiska
Dla ułatwienia, plik .env.example zastąpiony w tym projekcie został zawartością, jaką należy umieścic w docelowym pliku .env, wystarczy więc skopiuować go i zmienić jego nazwę na .env
Do działania wymagane jest jednak wygenerowania klucza aplikacji. Można to zrobić za pomocą polecenia:
```
php artisan key:generate
```
Klucz powinien automatycznie zostać umieszczony w pliku .env i innych wymaganych miejscach.

## Baza danych
Dla zmniejszenia narzutu konfiguracji przy testowaniu i konieczności stawiania bazy, wykorzystałem SQLite. Plik z danymi zlokalizowany jest w katalogu `/database/users.sqlite`

W pliku .env nalezy ustawić więc wartość `DB_CONNECTION` na `sqlite`
```
DB_CONNECTION=sqlite
```
Po stworzeniu pliku, aby utworzyć wymagane struktury wystarczy uruchomić poniższe polecenie. flaga `--seed` pozwoli na automatyczne wypełnienie bazy podstawowymi danymi takimi jak role, typy adresów, czy przykładowi uzytkownicy
```
php artisan migrate:fresh --seed
```

## Konfiguracja dostępu do katalogów
W pliku `composer.json` jest zdefiniowany skrypt `fix-storage`. Poprawia on uprawnienia do katalogu `storage` tak aby framework mógł zapisywać np logi. Skrypt został tez dodany do triggera `post-update-cmd`, więc w wiekszości przypadków automaty typu `composer install` czy `composer update` powinny na koniec wywołać go samodzielnie. Należy w skrypcie zmienić nazwę użytkownika w zależności od dystrubycji (apache, httpd, www-data etc.)

## Walidacje
Została dodana przykładowa walidacja, jeddnak ze względu na oszczędność czasu nie zostały zabezpieczone wszystkie sytuacje w których użytkownik może wprowadzić błędne dane

## Dokumentacja
Dokumentacja dostepna jest pod adresem http://localhost/api/documentation gdize `http://localhost` należy zamienić na domenę, na której hostowana będize aplikacja

## WIdoki VUE
Aby skompilować widoki w frameworku vue wykorzystanym w projekcie, należy w katalogu głównym projektu wykonać poniższe polecenie:
```
npm install & npm run dev
```

