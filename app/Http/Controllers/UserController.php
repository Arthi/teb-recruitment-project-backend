<?php

namespace App\Http\Controllers;

use App\Exceptions\SearchFieldEmptyException;
use App\Exceptions\UserValidationException;
use App\Models\Address;
use App\Models\Role;
use App\Models\Teacher;
use App\Models\User;
use App\Models\Worker;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserController extends Controller {

    public function __construct() {

        $this->middleware(function ($request, $next) {

            //do sth if needed //TODO usunac?

            return $next($request);
        });

    }

    public function view() {

        return view('users');

    }

    public function roles() {

        return Role::all();

    }

    public function list(Request $request) {

        $page = $request->query('page', 1);
        $size = $request->query('size', 10);

        return User::with('role')->get()
            ->sortBy(['last_name', 'first_name'])
            ->makeHidden(['role_id'])
            ->forPage($page, $size);

    }

    public function search(Request $request) {

        $search = $request->query('search', '');
        $page = $request->query('page', 1);
        $size = $request->query('size', 10);

        if (strlen($search < 3)) throw new BadRequestHttpException('Wyszukiwany tekst musi zawierac minimum 3 znaki');

        $users = User::with('role')
            ->where('last_name', 'LIKE', "%{$search}%")
            ->orWhere('first_name', 'LIKE', "%{$search}%")
            ->orWhere('email', 'LIKE', "%{$search}%")
            ->get()
            ->forPage($page, $size);

        return $users;
    }

    /**
     * @OA\Post(
     * path="/api/user",
     * operationId="createUser",
     * tags={"Create user"},
     * summary="Creating user",
     * description="Creating user and addiotional fields for teachers and workers",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *			    oneOf={
     *                  @OA\Schema(
     *                      description="Teacher create request",
     *                      title="Teacher",
     *                      type="object",
     *                      required={"firstName","lastName", "email", "password", "role"},
     *                      @OA\Property(property="firstName", type="string", format="text"),
     *                      @OA\Property(property="lastName", type="string", format="text"),
     *                      @OA\Property(property="email", type="string", format="email"),
     *                      @OA\Property(property="password", type="string", format="password"),
     *                      @OA\Property(property="role", type="integer", example="1"),
     *                      @OA\Property(property="phone", type="string"),
     *                      @OA\Property(property="education", type="string")
     *                  ),
     *				    @OA\Schema(
     *                      description="Worker create request",
     *                      title="Worker",
     *                      type="object",
     *                      required={"firstName","lastName", "email", "password", "role"},
     *                      @OA\Property(property="firstName", type="string", format="text"),
     *                      @OA\Property(property="lastName", type="string", format="text"),
     *                      @OA\Property(property="email", type="string", format="email"),
     *                      @OA\Property(property="password", type="string", format="password"),
     *                      @OA\Property(property="role", type="integer", example="1"),
     *                      @OA\Property(property="address_c", ref="#/components/schemas/Address"),
     *                      @OA\Property(property="address_l", ref="#/components/schemas/Address")
     *                  ),
     *
     *			  },
     *        ),
     *    ),
     *      @OA\Response(
     *          response=201,
     *          description="User created successfully",
     *          @OA\JsonContent(
     *			    oneOf={
     *                  @OA\Schema(ref="#/components/schemas/Worker"),
     *				    @OA\Schema(ref="#/components/schemas/Teacher"),
     *
     *			    },
     *		    ),
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Validation error - wrong data",
     *          @OA\JsonContent()
     *       )
     * )
     */
    public function create(Request $request) {

        try {

            $newUserData = (object)$request->all();

            $this->validatePerson($newUserData);

            $found = User::where('email', $newUserData->email)->count();

            if ($found > 0) throw new UserValidationException('Adres e-mail jest już zajęty');

            if ($newUserData->role == Role::Teacher) {

                $this->validateTeacher($newUserData);

                $user = Teacher::factory()->create([
                    'user_id' => User::factory()->teacher()->create([
                        'first_name' => $newUserData->firstName,
                        'last_name' => $newUserData->lastName,
                        'email' => $newUserData->email,
                        'password' => password_hash($newUserData->password, PASSWORD_BCRYPT)
                    ]),
                    'phone' => $newUserData->phone,
                    'education' => $newUserData->education
                ]);

            } else {

                $this->validateAddress($newUserData->address_c);
                $this->validateAddress($newUserData->address_l);

                $newUserData->address_c = (object)$newUserData->address_c;
                $newUserData->address_l = (object)$newUserData->address_l;

                $user = Worker::factory()->create([
                    'user_id' => User::factory()->worker()->create([
                        'first_name' => $newUserData->firstName,
                        'last_name' => $newUserData->lastName,
                        'email' => $newUserData->email,
                        'password' => password_hash($newUserData->password, PASSWORD_BCRYPT)
                    ])
                ]);

                Address::factory()->correspondence()
                    ->for($user)
                    ->create([
                        'voivodeship' => $newUserData->address_c->voivodeship,
                        'city' => $newUserData->address_c->city,
                        'postal_code' => $newUserData->address_c->code,
                        'street_name' => $newUserData->address_c->street,
                        'building_number' => $newUserData->address_c->number
                    ]);

                Address::factory()->living()
                    ->for($user)
                    ->create([
                        'voivodeship' => $newUserData->address_l->voivodeship,
                        'city' => $newUserData->address_l->city,
                        'postal_code' => $newUserData->address_l->code,
                        'street_name' => $newUserData->address_l->street,
                        'building_number' => $newUserData->address_l->number
                    ]);

            }

            return $user;

        } catch (UserValidationException $e) {

            return response(['error' => $e->getMessage()], 400);

        }

    }

    /**
     * @throws UserValidationException
     */
    private function validatePerson($newUserData) {

        if (!in_array(intval($newUserData->role), [Role::Teacher, Role::Worker])) throw new UserValidationException('Rola użytkownika jest niepoprawna');
        if ($newUserData->firstName == null || strlen($newUserData->firstName) < 3) throw new UserValidationException('Imię użytkownika jest niepoprawne');
        if ($newUserData->lastName == null || strlen($newUserData->lastName) < 3) throw new UserValidationException('Nazwisko użytkownika jest niepoprawne');
        if ($newUserData->email == null || filter_var($newUserData->email, FILTER_VALIDATE_EMAIL)) throw new UserValidationException('Email użytkownika jest niepoprawny');
        if ($newUserData->password == null || strlen($newUserData->password) < 10) throw new UserValidationException('Hasło użytkownika misi mieć minimum 10 znaków');

    }

    /**
     * @throws UserValidationException
     */
    private function validateTeacher($newUserData) {

        if ($newUserData->phone == null || strlen($newUserData->phone) != 9) throw new UserValidationException('Numer telefonu wykładowcy jest niepoprawny');
        if ($newUserData->education == null || strlen($newUserData->education) < 4) throw new UserValidationException('Wykształcenie wykładowcy misi mieć minimum 4 znaki');

    }

    /**
     * @throws UserValidationException
     */
    private function validateAddress($address) {

        if ($address == null) throw new UserValidationException('Adres jest wymagany!');

        $address = (object)$address;

        if ($address->voivodeship == null || strlen($address->voivodeship) <= 0) throw new UserValidationException('Województwo adresu jest niepoprawne');
        if (!isset($address->city) || $address->city == null || strlen($address->city) <= 3) throw new UserValidationException('Miasto adresu jest niepoprawne');
        if (!isset($address->code) || $address->code == null || strlen($address->code) != 6 || !str_contains($address->code, '-')) throw new UserValidationException('Kod pocztowy adresu jest niepoprawny');
        if (!isset($address->street) || $address->street == null || strlen($address->street) < 3) throw new UserValidationException('Nazwa ulicy jest niepoprawna ');
        if (!isset($address->number) || $address->number == null || strlen($address->number) < 1) throw new UserValidationException('Numer domu jest niepoprawny');

    }
}
