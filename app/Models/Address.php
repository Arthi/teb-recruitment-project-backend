<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @OA\Schema()
 * @OA\Property(property="voivodeship", format="integer", description="Voivodeship name"),
 * @OA\Property(property="city", format="integer", description="City name"),
 * @OA\Property(property="postal_code", format="integer", description="Postal code"),
 * @OA\Property(property="street_name", format="integer", description="Street name"),
 * @OA\Property(property="building_number", format="integer", description="Building number")
 */
class Address extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'voivodeship',
        'city',
        'postal_code',
        'street_name',
        'building_number'
    ];

    public function worker(): BelongsTo {

        return $this->belongsTo(Worker::class);

    }

    public function type(): HasOne {

        return $this->hasOne(AddressType::class);

    }


}
