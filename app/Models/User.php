<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @OA\Schema()
 * @OA\Property(property="user_id", format="int", description="User ID"),
 * @OA\Property(property="first_name", format="string", description="First name"),
 * @OA\Property(property="last_name", format="string", description="Last name"),
 * @OA\Property(property="email", format="email", description="E-mail"),
 * @OA\Property(property="password", format="password", description="Password"),
 * @OA\Property(property="role_id", format="integer", description="Role"),
 */
class User extends Authenticatable
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password'
    ];

    protected $hidden = [
        'password'
    ];

    public function role(): HasOne {

        return $this->hasOne(Role::class, 'id', 'role_id');

    }

}
