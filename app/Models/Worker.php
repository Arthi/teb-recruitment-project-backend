<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @OA\Schema()
 * @OA\Property(property="id", format="integer", description="Worker ID"),
 * @OA\Property(property="address", ref="#/components/schemas/Address"),
 */
class Worker extends User {

    use HasFactory;

    public $timestamps = false;

    protected $fillable = [];

    public function user(): HasOne {
        return $this->hasOne(User::class);
    }

    public function addresses(): HasMany {

        return $this->hasMany(Address::class);

    }

}
