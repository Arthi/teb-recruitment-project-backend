<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @OA\Schema()
 * @OA\Property(property="id", format="integer", description="Teacher ID"),
 * @OA\Property(property="phone", format="string", description="Phone"),
 * @OA\Property(property="education", format="string", description="Education"),
 */
class Teacher extends User {

    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'phone',
        'education'
    ];

    public function user(): HasOne {
        return $this->hasOne(User::class);
    }

//    protected static function boot()
//    {
//        parent::boot();
//
//        static::addGlobalScope('iscustomuser', function ($q) {
//            $q->where('role', Role::Custom);
//        });
//    }

}
