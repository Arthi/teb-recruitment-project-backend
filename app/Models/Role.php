<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Role extends Model
{
    use HasFactory;

    public const Teacher = 1;
    public const Worker = 2;

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

}
