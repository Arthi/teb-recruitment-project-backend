<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {

            $table->id();
            $table->string('voivodeship');
            $table->string('city');
            $table->string('postal_code');
            $table->string('street_name');
            $table->string('building_number');
            $table->unsignedBigInteger('worker_id');
            $table->foreign('worker_id')->references('id')->on('workers');
            $table->unsignedInteger('type_id');
            $table->foreign('type_id')->references('id')->on('address_types');

//            $table->unique(["worker_id", "type_id"], 'worker_address_unique');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');

//        Schema::table('addresses', function (Blueprint $table) {
//            $table->dropUnique('worker_address_unique');
//        });
        //TODO rozdzielić na dwa commity
    }
}
