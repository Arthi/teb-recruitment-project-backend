<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Worker;
use Illuminate\Database\Seeder;

class WorkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Worker::factory()
            ->has(Address::factory()->correspondence())
            ->has(Address::factory()->living())
            ->count(50000)
            ->create();
    }
}
