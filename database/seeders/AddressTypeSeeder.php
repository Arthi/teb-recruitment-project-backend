<?php

namespace Database\Seeders;

use App\Models\AddressType;
use Illuminate\Database\Seeder;

class AddressTypeSeeder extends Seeder
{

    public function run()
    {
        AddressType::create([
            'name' => 'corespondence'
        ]);

        AddressType::create([
            'name' => 'living'
        ]);
    }
}
