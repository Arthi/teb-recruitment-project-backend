<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{

    public function run()
    {
        Role::create([
            'id' => Role::Teacher,
            'name' => 'Wykładowca'
        ]);

        Role::create([
            'id' => Role::Worker,
            'name' => 'Pracownik administracyjny'
        ]);

    }
}
