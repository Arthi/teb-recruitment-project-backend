<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{

    /**
     * Define the model's default state.
     */
    public function definition(): array {

        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => password_hash($this->faker->password(10), PASSWORD_BCRYPT)
        ];
    }

    public function teacher(): UserFactory {

        return $this->state(function () {
            return [
                'role_id' => Role::Teacher
            ];
        });
    }

    public function worker(): UserFactory {

        return $this->state(function () {

            return [
                'role_id' => Role::Worker
            ];
        });
    }



}
