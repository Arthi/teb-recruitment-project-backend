<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'voivodeship' => $this->faker->monthName(),
            'city' => $this->faker->city(),
            'postal_code' => $this->faker->postcode(),
            'street_name' => $this->faker->streetName(),
            'building_number' => $this->faker->buildingNumber()
        ];
    }

    public function correspondence(): AddressFactory {

        return $this->state(function () {
            return [
                'type_id' => 1
            ];
        });
    }

    public function living(): AddressFactory {

        return $this->state(function () {
            return [
                'type_id' => 2
            ];
        });
    }

}
