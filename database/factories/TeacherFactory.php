<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TeacherFactory extends Factory
{

    public function definition(): array
    {

        return [
            'user_id' => User::factory()->teacher(),
            'phone' => $this->faker->phoneNumber(),
            'education' => 'University in ' . $this->faker->country()
        ];

    }
}
